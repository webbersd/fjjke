from api.services.get_random_idea_id_service import GetRandomIdeaIdService


class ApiActor:
    def __init__(self):
        self.get_random_idea_id_service = GetRandomIdeaIdService().get_random_idea_id()
