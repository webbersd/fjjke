from dataclasses import dataclass


@dataclass
class UserTokens:
    access_token: str
    refresh_token: str

    @staticmethod
    def as_user_token(dct):
        return UserTokens(dct['access_token'], dct['refresh_token'])
