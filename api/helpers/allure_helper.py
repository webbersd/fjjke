import json

import allure


def attach_json_response(response_json, name='Response'):
    allure.attach(json.dumps(response_json, indent=2), name=name, attachment_type=allure.attachment_type.JSON)
