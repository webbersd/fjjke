import json

import requests


def get_response_json(url):
    response = requests.get(url)
    if response.status_code == 200:
        result = True
        message = "Ok"
    else:
        result = False
        message = f"Error occurred: {response.status_code}"

    response_data = {
        "result": result,
        "message": message
    }
    return json.dumps(response_data)
