from api.services.get_tokens_service import GetTokensService


class RequestHeaders:
    @staticmethod
    def prepare_request_headers():
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer " + GetTokensService.login().access_token,
        }

        return headers
