import json
import unittest

import allure

from api.actors.api_actor import ApiActor
from api.config.config import config
from api.helpers.allure_helper import attach_json_response
from api.helpers.get_response_json import get_response_json


class TestWebviewPerformance(unittest.TestCase):
    def setUp(self):
        self.api_actor = ApiActor()

    def test_webview_performance(self):
        with allure.step('Test webview performance'):
            idea_id = self.api_actor.get_random_idea_id_service

            url = config.WEBVIEW_DEV_API_URL + f'/preview/en/invest-ideas/{idea_id}'

            response_json_string = get_response_json(url)
            response_json = json.loads(response_json_string)

            with allure.step("Verify response"):
                self.assertTrue(response_json["result"], response_json["message"])

            attach_json_response(response_json, name='Response')
