import logging
import random

import allure
import requests

from api.config.config import config
from api.helpers.request_headers import RequestHeaders


class GetRandomIdeaIdService:
    @staticmethod
    def get_random_idea_id():
        with allure.step('Getting random investment idea'):
            url = config.API_URL + "/v1/admin/invest-ideas/all"

            headers = RequestHeaders().prepare_request_headers()

            response = requests.get(url=url, headers=headers)

            assert response.status_code == 200

            user_dict = response.json()
            invest_idea_ids = [idea['invest_idea_id'] for idea in user_dict if 'invest_idea_id' in idea]

            if invest_idea_ids:
                random_invest_idea_id = random.choice(invest_idea_ids)
                logging.info(f"Randomly selected invest_idea_id: {random_invest_idea_id}")
            else:
                logging.error("No invest_idea_ids found.")

            return random_invest_idea_id
