import os


class Config:
    API_URL = os.getenv('API_URL')

    PASSWORD = os.getenv('ADMIN_PASSWORD')
    EMAIL = os.getenv('ADMIN_EMAIL')

    WEBVIEW_DEV_API_URL = os.getenv('WEBVIEW_DEV_API_URL')


config = Config()
